﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BranchSpawn : MonoBehaviour
{
    BranchPool BranchSp;

    void Awake()
    {
        BranchSp = GetComponent<BranchPool>();
    }

    void Update()
    {
        BranchSp.Generate();
    }
}
