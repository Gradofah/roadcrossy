﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawner : MonoBehaviour
{
    CoinPool coinSp;

    void Awake()
    {
        coinSp = GetComponent<CoinPool>();
    }

    void Update()
    {
        coinSp.Generate();
    }
}
