﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinCounter : MonoBehaviour
{
    public static int count = 0;
    public Text scoreCoin;

    void Start()
    {
        scoreCoin = GetComponent<Text>();
        count = PlayerPrefs.GetInt(Constants.COIN);
        
    }

    void Update()
    {
        scoreCoin.text = "x" + PlayerPrefs.GetInt(Constants.COIN).ToString();
    }
}
