﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BestScore : MonoBehaviour
{
    [SerializeField] private Text bestScore;

    void Start()
    {
        bestScore = GetComponent<Text>();
    }

    void Update()
    {
        if (!Player.die)
        {
            this.gameObject.SetActive(false);
        }
        bestScore.text = "Best: " + PlayerPrefs.GetInt(Constants.BEST_SCORE).ToString();
    }
}
