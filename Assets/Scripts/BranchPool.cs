﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BranchPool : MonoBehaviour
{
    [SerializeField] private GameObject branch;
    [SerializeField] private Transform spawnPoint;
    private int delay = 50;
    private List<GameObject> pool = new List<GameObject>();
    [SerializeField] private int amount;
    private bool isGrow = false;

    void Awake()
    {   
        for (int i = 0; i < amount; i++)
        {
            Create();
        }
    }

    GameObject Create()
    {
        GameObject temp = (GameObject) Instantiate(branch, spawnPoint.position, Quaternion.identity);
        temp.SetActive(false);
        pool.Add(temp);
        return temp;
    }

    GameObject GetPoolObj()
    {
        for (int i = 0; i <pool.Count; i++)
        {
            if (!pool[i].activeSelf)
            return pool[i];
        }

        if (isGrow)
        {
            return Create();
        }

        return null;
    }

    public void Generate()
    {   
        if (delay <= 0)
        {
            GameObject newBranch = GetPoolObj();
            if (newBranch != null)
            {
                newBranch.transform.position = spawnPoint.position;
                newBranch.SetActive(true);
                delay = 20;
            }
        }
    }

    void Update()
    {
        if (delay > 0)
        delay--;
    }
    
}
