﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGenerator : MonoBehaviour
{
  

    [SerializeField] private GameObject player;
    
    [SerializeField] private PlatformPool[] platformsP;
   
    int platformSelector;
    float shift = 1f;

    void Start()
    {
        for(int i = 0; i <10; i++)
        {   transform.position = new Vector3(transform.position.x+shift, transform.position.y, transform.position.z);           
            GameObject newPlatform = platformsP[0].GetPlatform();
            newPlatform.transform.position = transform.position;
            newPlatform.transform.rotation = transform.rotation;
            newPlatform.SetActive(true);
        }
    }

    void Update()
    {
        if(transform.position.x - player.transform.position.x < 15)
            {
                transform.position = new Vector3(transform.position.x+shift, transform.position.y, transform.position.z);
                platformSelector = Random.Range(0, platformsP.Length);
                
                GameObject newPlatform = platformsP[platformSelector].GetPlatform();

                newPlatform.transform.position = transform.position;
                newPlatform.transform.rotation = transform.rotation;
                newPlatform.SetActive(true);

                
            }
        

    }
}
