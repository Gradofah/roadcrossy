﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ScreenManager : MonoBehaviour
{
    [SerializeField] private GameObject startP, screenGame, screenLose;
    

    [SerializeField] private RectTransform startPanel;
    
    [SerializeField] private Button buttonPlay;
    public static bool isGameStart = false;

    void Start()
    {
        
        startPanel.DOAnchorPos(Vector2.zero, 0);
        
        buttonPlay.onClick.AddListener(OnButtonPlayClick);
    }

    void OnButtonPlayClick()
    {
        
        isGameStart = true;
        startP.SetActive(false);

    }

    void Update()
    {
        if (isGameStart == false)
            screenGame.SetActive(false);
        else if (isGameStart == true)
            screenGame.SetActive(true);

        if (Player.die == true)
        {
            screenLose.SetActive(true);
        }
    }

}
