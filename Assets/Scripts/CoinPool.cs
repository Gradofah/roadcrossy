﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPool : MonoBehaviour
{
   
    [SerializeField] private GameObject coin;
    public Transform spawnPoint, removePoint;
    int delay = 100;
    private List<GameObject> pool = new List<GameObject>();
    [SerializeField] private int amount;
    private bool isGrow = false;
    
    
    private Vector3 RandomRespawnPoint
    {
        get
        {
            Vector3 result = Vector3.zero;
            float randomZ = Mathf.Round(Random.Range(spawnPoint.position.z, removePoint.position.z));
            
            return new Vector3
            {
                x = transform.position.x,
                y = Constants.POZ_Y,
                z = randomZ
            };
        }        
    }   

    void Awake()
    {   
        for (int i = 0; i < amount; i++)
        {
            Create();
        }
    }

    GameObject Create()
    {
        GameObject temp = (GameObject) Instantiate(coin, spawnPoint.position, Quaternion.identity);
        temp.SetActive(false);
        pool.Add(temp);
        return temp;
    }

    GameObject GetPoolObj()
    {
        for (int i = 0; i <pool.Count; i++)
        {
            if (!pool[i].activeSelf)
            return pool[i];
        }

        if (isGrow)
        {
            return Create();
        }

        return null;
    }

    public void Generate()
    {
        if(delay <= 0)
        {
        GameObject newCoin = GetPoolObj();
            if (newCoin != null)
            {
                delay = 170;
               Vector3 point;
                do
                {
                    point = RandomRespawnPoint;
                }
                while(!IsClear(point));
                
                newCoin.transform.position = point;

                newCoin.SetActive(true);               
            }
        }
    }

    bool IsClear(Vector3 checkablePoint)
    {
        RaycastHit hit = new RaycastHit();

            if(Physics.Raycast(checkablePoint, checkablePoint + Vector3.up, out hit, 0.1f))
            {
                return false;
            } 

            return true;
    }

    void Update()
    {
        if (delay > 0)
            delay--;
    }

}
