﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants
{
   public  const string SOUND_CHECK = "sound" ;
   public const string SCORE_CHECK = "maxWay";
   public  const string BEST_SCORE = "maxScore";
   public const string WATER_WALL = "WaterWall";
   public  const string WATER = "WaterBlock";
   public const string CAR = "Car";
   public  const string COIN ="Coin";
   public const string LOG = "Log";
   public  const string PLAYER = "Player";

   public const string WALL = "Wall";
   public const string GROUND = "Ground";
   public const float POZ_Y = 0.5f;

   
}
