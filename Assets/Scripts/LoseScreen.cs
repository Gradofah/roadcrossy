﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class LoseScreen : MonoBehaviour
{
    [SerializeField] private Button restartButton, soundButton;
    [SerializeField] private Sprite soundOn, soundOff;
    [SerializeField] private Image imageSound;

  
    [SerializeField] private AudioSource _click;
    [SerializeField] private GameObject player;
    [SerializeField] private Player playerInstance;


    void Start()
    {
        _click = GetComponent<AudioSource>();
   
        soundButton.onClick.AddListener(OnSoundClicked);
        restartButton.onClick.AddListener(OnRestartClicked);

        gameObject.SetActive(Player.die);

        ChangeSoundButtonVisual();
    }

    private void OnEnable()
    {
        Player.OnDied += Player_OnDied;
    }

    private void OnDisable()
    {
        Player.OnDied -= Player_OnDied;
    }

    void OnRestartClicked()
    {
        playerInstance.Respawn();
        Controll.backSteps = 0;
        player.transform.DOMove(new Vector3(1, 0.5f, 0), 0, false);
        player.transform.DORotate(new Vector3(0, 90, 0), 0, RotateMode.Fast);
        player.SetActive(true);
        ScoreCount.scoreWay.text = "0";
        ScoreCount.count = 0;
        
        PlayerPrefs.SetFloat(Constants.SCORE_CHECK, 0);
        _click.Play();
    }

   void OnSoundClicked()
    {       
      if (PlayerPrefs.GetInt(Constants.SOUND_CHECK) == 0)
        {
            PlayerPrefs.SetInt(Constants.SOUND_CHECK, 1);
        }  
      else if (PlayerPrefs.GetInt(Constants.SOUND_CHECK) == 1)
        {
            PlayerPrefs.SetInt(Constants.SOUND_CHECK, 0);
        }

        _click.Play();

        ChangeSoundButtonVisual();
    }

    private void Player_OnDied(bool isDied)
    {
        gameObject.SetActive(isDied);
    }    

    private void ChangeSoundButtonVisual()
    {
        if (PlayerPrefs.GetInt(Constants.SOUND_CHECK) == 1)
        {
            imageSound.sprite = soundOff;
            AudioListener.volume = 0;
        }
        else if (PlayerPrefs.GetInt(Constants.SOUND_CHECK) == 0)
        {
            imageSound.sprite = soundOn;
            AudioListener.volume = 1;
        }
    }
}

