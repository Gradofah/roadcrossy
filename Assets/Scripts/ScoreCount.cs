﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCount : MonoBehaviour
{
    public static int count;
    public static Text scoreWay;

    void Start()
    {
        scoreWay = GetComponent<Text>();
        PlayerPrefs.SetFloat(Constants.SCORE_CHECK, 0);
        count=0;
    }

    void Update()
    {
        scoreWay.text = count.ToString();
    }
}
