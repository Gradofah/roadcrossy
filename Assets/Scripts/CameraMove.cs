﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    public GameObject player;
    public GameObject camera;
    Vector3 startDistance = new Vector3(-8f, 16f, -3.5f);
    Vector3 move;

   
    void Update()
    {
        move = player.transform.position + startDistance;
        move.y = startDistance.y;
        
        camera.transform.position = move;

    }
}
