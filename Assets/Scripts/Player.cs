﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;

public class Player : MonoBehaviour
{   
    public static bool die = false;

    public static event Action<bool> OnDied;
    
    [SerializeField] GameObject LoseScreen, BestScore;
    [SerializeField] AudioSource  sound;

    void Start()
    {
        sound = GetComponent<AudioSource>();
    }

    public void Respawn()
    {
        die = false;
       
        OnDied?.Invoke(die);
    }
    

    
    void OnTriggerEnter(Collider col)
    {
        if(col.CompareTag(Constants.COIN))
        {
            CoinCounter.count++;
            col.gameObject.SetActive(false);
            PlayerPrefs.SetInt(Constants.COIN, CoinCounter.count);
            sound.Play();
            
        }

        if(col.CompareTag(Constants.WATER) || col.CompareTag(Constants.CAR) || col.CompareTag(Constants.WATER_WALL))
        {
            this.gameObject.SetActive(false);
            this.transform.SetParent(null);
            die = true;
            LoseScreen.gameObject.SetActive(true);

           

            OnDied?.Invoke(die);
            
        }

       
        
    }

    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.CompareTag(Constants.LOG))
        {
            this.transform.SetParent(col.transform);
        }
    }

    void OnCollisionExit(Collision col)
    {
        if(col.gameObject.CompareTag(Constants.LOG))
        {
            this.transform.SetParent(null);
            
        }
    }

    
    
}
