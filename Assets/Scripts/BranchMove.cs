﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BranchMove : MonoBehaviour
{
    [SerializeField] private GameObject br;
    Tweener branch;
    [SerializeField] float min, max;
    void Start()
    {
        branch = transform.DOLocalMoveZ(30f, Random.Range(min, max), false).OnComplete(FinishItem);
    }

   void FinishItem()
   {
        br.gameObject.SetActive(false);
        branch.Restart(true, Random.Range(3,6));
   }
   
}
