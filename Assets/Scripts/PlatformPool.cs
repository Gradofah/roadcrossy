﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformPool : MonoBehaviour
{
    [SerializeField] private GameObject platform;

    [SerializeField] private int amount;

    private List<GameObject> platforms;

    void Start()
    {
        platforms = new List<GameObject>();

        for (int i = 0; i < amount; i++)
        {
            GameObject obj = (GameObject) Instantiate(platform);
            obj.SetActive(false);
            platforms.Add(obj);
        }
    }
    

    public GameObject GetPlatform()
    {
        for (int i = 0; i < platforms.Count; i++)
        {
            if(!platforms[i].activeInHierarchy)
            {
                return platforms[i];
            }
        }

        GameObject obj = (GameObject) Instantiate(platform);
        obj.SetActive(false);
        platforms.Add(obj);
        return obj;
    }
}
