﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPlay : MonoBehaviour
{
    [SerializeField] AudioSource sound;

    void Start()
    {
        sound = GetComponent<AudioSource>();
        
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag(Constants.PLAYER))
        sound.Play();
    }
}
