﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeSpawner : MonoBehaviour
{
    TreePool treeSp;

    void Awake()
    {
        treeSp = GetComponent<TreePool>();
    }

    void Update()
    {
        treeSp.Generate();
    }
}
