﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficCheck : MonoBehaviour
{
    [SerializeField] GameObject sphere;

    void Start()
    {
        sphere.GetComponent<Renderer>().material.color = Color.green;
    }
    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag(Constants.CAR))
        {
            sphere.GetComponent<Renderer>().material.color = Color.red;
        }
    }

     void OnTriggerExit(Collider col)
    {
        if (col.CompareTag(Constants.CAR))
        {
            sphere.GetComponent<Renderer>().material.color = Color.green;
        }
    }
}
