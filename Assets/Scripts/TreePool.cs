﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreePool : MonoBehaviour
{
   
    [SerializeField] private GameObject tree;
    [SerializeField] private Transform spawnPoint, removePoint;
    
    private List<GameObject> pool = new List<GameObject>();
    [SerializeField] private int amount;
    private bool isGrow = false;

    

    
        
    

    private Vector3 RandomRespawnPoint
    {
        get
        {
            Vector3 result = Vector3.zero;
            float randomZ = Mathf.Round(Random.Range(spawnPoint.position.z, removePoint.position.z));
            
            return new Vector3
            {
                x = transform.position.x,
                y = Constants.POZ_Y,
                z = randomZ
            };
        }        
    }   
    void Awake()
    {   
        for (int i = 0; i < amount; i++)
        {
            Create();
        }
    }

    GameObject Create()
    {
        GameObject temp = (GameObject) Instantiate(tree, spawnPoint.position, Quaternion.identity);
        temp.SetActive(false);
        pool.Add(temp);
        return temp;
    }

    GameObject GetPoolObj()
    {
        for (int i = 0; i <pool.Count; i++)
        {
            if (!pool[i].activeSelf)
            return pool[i];
        }

        if (isGrow)
        {
            return Create();
        }

        return null;
    }

    public void Generate()
    {        
        GameObject newTree = GetPoolObj();
            if (newTree != null)
            {   
                Vector3 point;
                do
                {
                    point = RandomRespawnPoint;
                }
                while(!IsClear(point));
                
                newTree.transform.position = point;

                newTree.SetActive(true);               
            }
        
    }

    bool IsClear(Vector3 checkablePoint)
    {
        RaycastHit hit = new RaycastHit();

            if(Physics.Raycast(checkablePoint, checkablePoint + Vector3.up, out hit, 0.1f))
            {
                return false;
            } 

            return true;
    }


}
