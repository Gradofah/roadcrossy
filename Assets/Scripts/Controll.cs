﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class Controll : MonoBehaviour, IBeginDragHandler, IDragHandler
{
    [SerializeField] private GameObject backStepsLose;
    [SerializeField] private  GameObject player;
    [SerializeField] private AudioSource pl;
    [SerializeField] private float dur;
    public static int backSteps = 0;

    private const float shift = 1f;
    
    
    

    void Start()
    {
        pl = GetComponent<AudioSource>();
    }
    public void OnBeginDrag(PointerEventData eventData)
    {   
        if (ScreenManager.isGameStart && !Player.die && IsGrounded)
        {          
            if ((Mathf.Abs(eventData.delta.x)) > (Mathf.Abs(eventData.delta.y)))
                {
                    if (eventData.delta.x > 0)
                        {
                            
                            MoveRight();
                        }
                        else 
                        {
                            MoveLeft();
                        }  
                }
                else 
                {
                    if (eventData.delta.y > 0)
                    {
                        MoveForward();
                    }
                    else 
                    { 
                        MoveBack();
                    }
                }
        }
    } 
    public void OnDrag(PointerEventData eventData)
    {

    }  

    
    void Update()
    {
        if (backSteps == 2)
        {
            Player.die = true;
            player.gameObject.SetActive(false);
            backStepsLose.gameObject.SetActive(true);      
        }
        else 
        {
            backStepsLose.gameObject.SetActive(false);
        }
        
    }
   
   void MoveForward()
   {
        player.transform.DORotate(new Vector3(0, 90, 0), dur, RotateMode.Fast).OnComplete(() =>
        {
            if(!IsWall)
            {
                player.transform.DOJump(new Vector3(Mathf.Round(player.transform.position.x) + shift, player.transform.position.y, Mathf.Round(player.transform.position.z)), 1f, 1, dur, false);
                backSteps = 0;
                if (PlayerPrefs.GetFloat(Constants.SCORE_CHECK) < player.transform.position.x)
                {
                    ScoreCount.count++;
                    PlayerPrefs.SetFloat(Constants.SCORE_CHECK, player.transform.position.x);
                }
                if (ScoreCount.count > PlayerPrefs.GetInt(Constants.BEST_SCORE))
                    PlayerPrefs.SetInt(Constants.BEST_SCORE, ScoreCount.count);

                pl.Play();
            }
        });     
   }

   void MoveBack()
   {
        player.transform.DORotate(new Vector3(0, 270, 0), dur, RotateMode.Fast).OnComplete(() =>
        {
            if(!IsWall)
            {
                player.transform.DOJump(new Vector3(Mathf.Round(player.transform.position.x) - shift, player.transform.position.y, Mathf.Round(player.transform.position.z)), 1f, 1, dur, false);
                backSteps++;

                pl.Play();
            }
        });     
   }  

   void MoveRight()
   {
        player.transform.DORotate(new Vector3(0, 180, 0), dur, RotateMode.Fast).OnComplete(() =>
        {
            if(!IsWall)
            {
                player.transform.DOJump(new Vector3(Mathf.Round(player.transform.position.x), player.transform.position.y, Mathf.Round(player.transform.position.z) - shift), 1f, 1, dur, false);

                pl.Play();
            }
        });     
   }

   void MoveLeft()
   {
        player.transform.DORotate(new Vector3(0, 360, 0), dur, RotateMode.Fast).OnComplete(() =>
        {
            if(!IsWall)
            {
                player.transform.DOJump(new Vector3(Mathf.Round(player.transform.position.x), player.transform.position.y, Mathf.Round(player.transform.position.z) + shift), 1f, 1, dur, false);
                
                pl.Play();
            }

        });     
   }

    bool IsWall
    {
        get
        {
            RaycastHit hit = new RaycastHit();
            int layerMask = LayerMask.GetMask(Constants.WALL);

            if (Physics.Raycast(player.transform.position, player.transform.forward+player.transform.up*0.5f, out hit, 1f,layerMask ))
            {
                return true;
            }
            return false;
                    
        }
    }

    bool IsGrounded
    {
        get
        {
            RaycastHit hit = new RaycastHit();
            int layerMask = LayerMask.GetMask(Constants.GROUND);
            
            if(Physics.Raycast(player.transform.position, -player.transform.up, out hit, 0.05f, layerMask))
            {
                return true;
            } 
            return false;
        }
    }
}
